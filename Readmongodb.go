package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Trainer struct {
	Type  string
	Name  string
	Price string
}

func Readmongodb() {

	//brew services start mongodb-community@4.4

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	collection := client.Database("MenuDB").Collection("SubMenu")

	findOptions := options.Find()
	findOptions.SetLimit(2)

	//var results []Trainer
	var results []SubMenu
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.D{{}})
	if err != nil {
		panic(err)
	}
	if err = cursor.All(ctx, &results); err != nil {
		panic(err)
	}
	fmt.Println(results[0].Drink[0])

}
