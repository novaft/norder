package main

import (
	"database/sql"
	"fmt"
	"time"
)

//OrderSecCheck check the key and table is valid
func OrderSecCheck(SecKey string, ReqTab string, db *sql.DB) (bool, int) {
	currentTime := time.Now()
	result := false
	tableID := 0
	stmt, err := db.Prepare("SELECT ID,ReqTime from TableReq where QrKey=? and TableNo=?")
	rows, err := stmt.Query(SecKey, ReqTab)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Select %s\r\n", err)
		}
	}()

	checkErr(err)
	if rows.Next() {
		var ID int
		var ReqTime string
		err = rows.Scan(&ID, &ReqTime)
		reqTime, _ := time.ParseInLocation("2006-01-02 15:04", ReqTime, time.Local)

		exphr, _ := time.ParseDuration("10000h")
		expTime := reqTime.Add(exphr)
		if expTime.After(currentTime) && ID > 0 {
			result = true
			tableID = ID
		}
	}

	stmt.Close()
	rows.Close()
	return result, tableID
}
