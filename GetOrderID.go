package main

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
)

//GetOrderID retieve Table Order
func getOrderID(c echo.Context) (err error) {
	u := new(ReqInput)
	if err = c.Bind(u); err != nil {
		return
	}
	db, _ := sql.Open("mysql", dbcon())
	result := DbOrderIDPro(u.TableID, db)
	db.Close()
	return c.String(http.StatusOK, result)
}

func DbOrderIDPro(tableID string, db *sql.DB) string {
	stmt, _ := db.Prepare("SELECT ID,ReqTime from TableReq where TableNo = ? Limit 50")
	rows, err := stmt.Query(tableID)
	checkErr(err)
	var data []OrderIDData
	for rows.Next() {
		var ID string
		var OrDate string
		err = rows.Scan(&ID, &OrDate)
		data = append(data, OrderIDData{
			ID:     ID,
			OrDate: OrDate,
		})
	}
	pushdata, _ := json.Marshal(data)
	result := string(pushdata)
	stmt.Close()
	rows.Close()
	db.Close()
	return result
}
