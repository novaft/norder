package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {

	e := echo.New()

	e.GET("/reqtcat/:catid", reqCat)
	e.POST("/checkReqKey", checkReqKey)
	e.POST("/reqMenu", reqMenu)
	e.GET("/reqTypeGrid", reqTypeGrid)
	e.GET("/reqType", reqType)
	e.POST("/orderSum", orderSum)
	e.POST("/getOrderID", getOrderID)
	e.POST("/orderReceiver", orderReceiver)
	e.GET("/recommend", recommend)
	e.Static("/Assets", "Assets")
	e.Use(middleware.CORS())
	e.Logger.Fatal(e.Start(":9090"))

	//Enable CORS for Local React Getting data
	/*
		mux := http.NewServeMux()
		mux.HandleFunc("/recommend", recommend)
		mux.HandleFunc("/reqtcat", reqCat)
		mux.HandleFunc("/reqMenu", reqMenu)
		mux.HandleFunc("/reqType", reqType)
		mux.HandleFunc("/orderReceiver", orderReceiver)
		mux.HandleFunc("/checkReqKey", checkReqKey)
		mux.HandleFunc("/reqTypeGrid", reqTypeGrid)
		mux.HandleFunc("/OrderSum", OrderSum)
		mux.HandleFunc("/GetOrderID", GetOrderID)

		mux.Handle("/Assets/", http.StripPrefix("/Assets/", http.FileServer(http.Dir("./Assets"))))
		//err := http.ListenAndServeTLS(":9090", "server.crt", "server.key", nil)
		handler := cors.Default().Handler(mux)
		http.ListenAndServe(":9090", handler)
	*/

}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
