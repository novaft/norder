package main

import (
	"database/sql"
	"fmt"
)

//OrderSecVipCheck check the key and table is valid
func OrderSecVipCheck(SecKey string, ReqTab string, db *sql.DB) int {
	result := 0
	stmt, err := db.Prepare("SELECT VIP from TableReq where QrKey=? and TableNo=?")
	checkErr(err)
	rows, err := stmt.Query(SecKey, ReqTab)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Select VIP Error %s\r\n", err)
		}
	}()

	if rows.Next() {
		var VIP int
		err = rows.Scan(&VIP)
		result = VIP
	}
	stmt.Close()
	rows.Close()
	return result
}
