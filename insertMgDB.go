package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DrinkA struct {
	Name  string
	Price float32
}

type SnackA struct {
	Name  string
	Price float32
}

type SubMenu struct {
	Drink []DrinkA
	Snack []SnackA
}

func InsertMgDB() {

	//brew services start mongodb-community@4.4

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	collection := client.Database("MenuDB").Collection("SubMenu")

	findOptions := options.Find()
	findOptions.SetLimit(2)

	//var results []Trainer

	a := DrinkA{Name: "熱奶茶", Price: 123}
	b := DrinkA{Name: "凍啡", Price: 3}
	var Drink []DrinkA
	Drink = append(Drink, a)
	Drink = append(Drink, b)

	c := SnackA{Name: "豆腐", Price: 123}
	d := SnackA{Name: "菜", Price: 3}
	var Snack []SnackA
	Snack = append(Snack, c)
	Snack = append(Snack, d)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.InsertOne(ctx, bson.D{
		{Key: "Name", Value: "Set2"},
		{Key: "Drink", Value: Drink},
		{Key: "Snack", Value: Snack},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(cursor.InsertedID)

}
