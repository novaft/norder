package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

//ReqCat request by React and return the food details

func reqMenu(c echo.Context) (err error) {
	u := new(ReqMenuInputArray)
	if err = c.Bind(u); err != nil {
		return
	}
	ReqCatIDResult := PushReqMenuResult(u.CatID, u.ReceiverOrderSecInput)
	return c.String(http.StatusOK, ReqCatIDResult)

}

//PushReqMenuResult DB process
func PushReqMenuResult(CatID string, SecInput []ReceiverOrderSecInput) string {
	var result string
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)
	var RDA RecDataArray
	var pushdata []byte
	VIPCheck := OrderSecVipCheck(SecInput[0].ReqKey, SecInput[0].ReqTab, db)
	stmt, err := db.Prepare("SELECT ID,Type,Name,ImgAddr from Food where Type =? and (VIP=0 or VIP=?)")
	rows, err := stmt.Query(CatID, VIPCheck)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panicing %s\r\n", err)
		}
	}()
	checkErr(err)
	for rows.Next() {
		var ID string
		var Type string
		var Name string
		var ImgAddr string
		err = rows.Scan(&ID, &Type, &Name, &ImgAddr)
		RDA.RecData = append(RDA.RecData, RecData{ID: ID, Type: Type, Name: Name, ImgAddr: ImgAddr})
	}

	pushdata, err = json.Marshal(RDA)
	result = string(pushdata)
	stmt.Close()
	rows.Close()

	db.Close()
	return result
}
