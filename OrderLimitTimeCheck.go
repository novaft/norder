package main

import (
	"database/sql"
	"fmt"
	"time"
)

//OrderLimitTimeCheck check the Time
func OrderLimitTimeCheck(ReqTab string, db *sql.DB) (bool, int) {
	currentTime := time.Now()
	mins := 0
	result := false
	stmt, err := db.Prepare("SELECT OrDate from orderRec where OrderTab=? ORDER BY ID DESC Limit 1")
	rows, err := stmt.Query(ReqTab)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Select %s\r\n", err)
		}
	}()
	checkErr(err)
	if rows.Next() {
		var OrDate string
		err = rows.Scan(&OrDate)
		LastOrderTime, _ := time.ParseInLocation("2006-01-02 15:04", OrDate, time.Local)
		AddTime, _ := time.ParseDuration("15m")
		LimitTime := LastOrderTime.Add(AddTime)

		if currentTime.After(LimitTime) {
			result = true
		}
		difference := LimitTime.Sub(currentTime)
		mins = int(difference.Minutes())
		if mins < 1 {
			mins = 1
		}
	} else {
		result = true
	}
	stmt.Close()
	rows.Close()
	return result, mins
}
