package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

func reqTypeGrid(c echo.Context) error {
	ReqTypeGridResult := PushReqTypeGridResult()
	return c.String(http.StatusOK, ReqTypeGridResult)
}

//PushReqTypeGridResult return Index Grid data
func PushReqTypeGridResult() string {
	var result string
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)
	var TGD TypeGridDataArray
	var pushdata []byte

	stmt, err := db.Prepare("SELECT ID,Name,ImgAddr from FoodType")
	checkErr(err)
	rows, err := stmt.Query()
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panicing %s\r\n", err)
		}
	}()
	checkErr(err)
	for rows.Next() {
		var ID string
		var Name string
		var ImgAddr string
		err = rows.Scan(&ID, &Name, &ImgAddr)
		TGD.TypeGridData = append(TGD.TypeGridData, TypeGridData{ID: ID, Name: Name, ImgAddr: ImgAddr})
	}
	pushdata, err = json.Marshal(TGD)
	result = string(pushdata)
	stmt.Close()
	rows.Close()

	db.Close()
	return result
}
