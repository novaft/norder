package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

func recommend(c echo.Context) (err error) {

	RecResult := pushRecResult()
	return c.String(http.StatusOK, RecResult)

}

func pushRecResult() string {
	var result string
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)
	var RDA RecDataArray
	var pushdata []byte

	stmt, err := db.Prepare("SELECT ID,Type,Name,ImgAddr from Food")
	rows, err := stmt.Query()
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panicing %s\r\n", err)
		}
	}()
	checkErr(err)
	for rows.Next() {
		var ID string
		var Type string
		var Name string
		var ImgAddr string
		err = rows.Scan(&ID, &Type, &Name, &ImgAddr)

		RDA.RecData = append(RDA.RecData, RecData{ID: ID, Type: Type, Name: Name, ImgAddr: ImgAddr})
	}

	pushdata, err = json.Marshal(RDA)
	result = string(pushdata)
	stmt.Close()
	rows.Close()

	db.Close()
	return result
}
