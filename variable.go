package main

type RecData struct {
	ID      string
	Type    string
	Name    string
	ImgAddr string
}
type RecDataArray struct {
	RecData []RecData
}

type TypeData struct {
	ID   string
	Name string
}
type TypeDataArray struct {
	TypeData []TypeData
}

type TypeGridData struct {
	ID      string
	Name    string
	ImgAddr string
}
type TypeGridDataArray struct {
	TypeGridData []TypeGridData
}
type ReceiverFoodInput struct {
	ID     string
	Type   string
	Name   string
	Amount int
}
type ReceiverOrderTableInput struct {
	OrderTable string
}
type ReceiverOrderSecInput struct {
	ReqKey string
	ReqTab string
}

type ReqMenuInputArray struct {
	ReceiverOrderSecInput []ReceiverOrderSecInput
	CatID                 string
}

type ReceiverInputArray struct {
	ReceiverFoodInput       []ReceiverFoodInput
	ReceiverOrderTableInput []ReceiverOrderTableInput
	ReceiverOrderSecInput   []ReceiverOrderSecInput
}
type DBOrderData struct {
	ID         string
	Name       string
	Amount     string
	OrderTable string
	OrderTime  string
	PrGroup    string
}

type DBOrderArray struct {
	DBOrderData []DBOrderData
}

type SecKeyData struct {
	ReqKey string
	ReqTab string
}
type SecKeyDataDataArray struct {
	SecKeyData []SecKeyData
}

type ReqInput struct {
	TableID string
}

type OrderData struct {
	ID     string
	Name   string
	Amount string
}
type OrderIDData struct {
	ID     string
	OrDate string
}
