package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

//ReqCat request by React and return the food details
//example http://127.0.0.1:9090/reqtcat?catid=1

func reqCat(c echo.Context) error {
	catid := c.Param("catid")
	ReqCatIDResult := PushReqCatIDResult(catid)
	return c.String(http.StatusOK, ReqCatIDResult)
	//fmt.Fprintf(w, string(ReqCatIDResult))

}

//PushReqCatIDResult DB process
func PushReqCatIDResult(catid string) string {
	var result string
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)
	var RDA RecDataArray
	var pushdata []byte

	stmt, err := db.Prepare("SELECT ID,Type,Name,ImgAddr from Food where Type =?")
	rows, err := stmt.Query(catid)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panicing %s\r\n", err)
		}
	}()
	checkErr(err)
	for rows.Next() {
		var ID string
		var Type string
		var Name string
		var ImgAddr string
		err = rows.Scan(&ID, &Type, &Name, &ImgAddr)

		RDA.RecData = append(RDA.RecData, RecData{ID: ID, Type: Type, Name: Name, ImgAddr: ImgAddr})
	}

	pushdata, err = json.Marshal(RDA)
	result = string(pushdata)
	stmt.Close()
	rows.Close()

	db.Close()
	return result
}
