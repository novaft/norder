package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

func orderReceiver(c echo.Context) (err error) {
	Result := "false"
	u := new(ReceiverInputArray)
	if err = c.Bind(u); err != nil {
		return
	}
	RecResult, TimeLeft := receiverProcess(u.ReceiverOrderSecInput, u.ReceiverFoodInput, u.ReceiverOrderTableInput)
	finalTimeLeft := strconv.Itoa(TimeLeft)
	switch RecResult {
	case 1:
		Result = "點餐成功"
	case 2:
		Result = "請在" + finalTimeLeft + "分鐘之後再點餐"
	}
	return c.String(http.StatusOK, Result)
}

func receiverProcess(recData []ReceiverOrderSecInput, recFood []ReceiverFoodInput, recTable []ReceiverOrderTableInput) (int, int) {
	db, err := sql.Open("mysql", dbcon())
	if err != nil {
		log.Fatal(err)
	}
	InsertDbResult := 0
	currentdate := time.Now()
	finalcurrent := currentdate.Format("15:04")
	finalcurrentfull := currentdate.Format("2006-01-02 15:04")
	//Received data Sample
	//[{"ReceiverOrderTableInput":[{"OrderTable":"1"}],"ReceiverFoodInput":[{"ID":"1","Name":"三文魚","Amount": "1","Type":"2"},{"ID":"6","Name":"油甘魚","Amount": "2","Type":"2"}] } ]
	//final
	//[{"Name":"三文魚","Amount": "1","OrderTable":"1","OrderTime":"10:30","PrGroup":"0"}]
	var FoodArray []DBOrderData
	var pushdata []byte

	ReqKey := recData[0].ReqKey

	ReqTab := recData[0].ReqTab

	orderSecCheck, TableID := OrderSecCheck(ReqKey, ReqTab, db)
	orderLimitTimeCheck, orderLimitLeft := OrderLimitTimeCheck(ReqTab, db)

	if orderSecCheck {

		if orderLimitTimeCheck {

			query1, err := db.Prepare("Select PrGroup from FoodType where ID=?")
			checkErr(err)
			for _, arrResult := range recFood {
				rows, err := query1.Query(arrResult.Type)
				if rows.Next() {
					var PrGroup string
					err = rows.Scan(&PrGroup)
					checkErr(err)
					arrAmount := strconv.Itoa(arrResult.Amount)
					FoodArray = append(FoodArray, DBOrderData{
						ID:         arrResult.ID,
						Name:       arrResult.Name,
						Amount:     arrAmount,
						OrderTable: recTable[0].OrderTable,
						OrderTime:  finalcurrent,
						PrGroup:    PrGroup,
					})
				}
				rows.Close()
			}
			query1.Close()
			pushdata, err = json.Marshal(FoodArray)

			InsertDbResult = DBInsertProcess(string(pushdata), finalcurrentfull, recTable[0].OrderTable, TableID)
		} else {
			//order request less than 15mins compare with last order
			InsertDbResult = 2
		}

	}

	db.Close()
	return InsertDbResult, orderLimitLeft
}

//DBInsertProcess Insert Order to DB
func DBInsertProcess(recData string, OrTime string, OrTable string, TableID int) int {

	insertResult := 0
	db, err := sql.Open("mysql", dbcon())
	if err != nil {
		log.Fatal(err)
	}
	db2, err := sql.Open("mysql", dbForCache())
	if err != nil {
		log.Fatal(err)
	}

	insertorder, err := db.Prepare("insert orderRec set OrData=?,OrDate=?,OrderTab=?,TableReqID=?")
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("insert order %s\r\n", err)
		}
	}()
	checkErr(err)
	inserttocache, err := db2.Prepare("insert orderRec set OrData=?,OrDate=?")
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("insert to cache %s\r\n", err)
		}
	}()
	checkErr(err)
	Result, err := insertorder.Exec(recData, OrTime, OrTable, TableID)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Result 1 %s\r\n", err)
		}
	}()
	checkErr(err)
	Result2, err := inserttocache.Exec(recData, OrTime)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Result 2 %s\r\n", err)
		}
	}()
	checkErr(err)
	affect, err := Result.RowsAffected()
	checkErr(err)
	affect2, err := Result2.RowsAffected()
	checkErr(err)

	if affect == 1 && affect2 == 1 {
		insertResult = 1
	}
	insertorder.Close()
	inserttocache.Close()
	db.Close()
	db2.Close()
	return insertResult
}
