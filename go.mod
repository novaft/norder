module nordering

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/labstack/echo/v4 v4.1.16
	go.mongodb.org/mongo-driver v1.4.0
)
