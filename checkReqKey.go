package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

//checkReqKey
//example http://127.0.0.1:9090/checkReqKey
//[{"SecKeyData":[{"ReqKey":"12692489d9075d1cdf48","ReqTab":"1"}]}]
func checkReqKey(c echo.Context) (err error) {

	u := new(SecKeyData)
	if err = c.Bind(u); err != nil {
		return
	}
	ReqTabResult := ReqKeyCheck(u.ReqKey, u.ReqTab)
	return c.JSON(http.StatusOK, ReqTabResult)
}

//ReqKeyCheck check the key and table is valid
func ReqKeyCheck(ReqKey string, ReqTab string) string {
	currentTime := time.Now()
	result := "0"
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)

	stmt, err := db.Prepare("SELECT ID,ReqTime from TableReq where QrKey=? and TableNo=?")
	rows, err := stmt.Query(ReqKey, ReqTab)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Select TableReq Error %s\r\n", err)
		}
	}()
	checkErr(err)

	if rows.Next() {

		var ID int
		var ReqTime string
		err = rows.Scan(&ID, &ReqTime)
		reqTime, _ := time.ParseInLocation("2006-01-02 15:04", ReqTime, time.Local)
		exphr, _ := time.ParseDuration("5000h")
		expTime := reqTime.Add(exphr)

		if expTime.After(currentTime) && ID > 0 {
			result = "1"
		}

	}

	stmt.Close()
	rows.Close()
	db.Close()
	return result
}
