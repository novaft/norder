package main

import (
	"database/sql"
)

//GetTabRecLen return record count of orderRec
func GetTabRecLen(db *sql.DB, tableID string) int {
	stmt, _ := db.Prepare("Select count(*) as count from orderRec where TableReqID = ?")
	rows, err := stmt.Query(tableID)
	checkErr(err)
	counter := 0
	if rows.Next() {
		var count int
		err := rows.Scan(&count)
		checkErr(err)
		counter = count
	}
	rows.Close()
	stmt.Close()
	return counter
}
