package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

//OrderSum for summarize the order
func orderSum(c echo.Context) (err error) {

	u := new(ReqInput)
	if err = c.Bind(u); err != nil {
		return
	}

	db, _ := sql.Open("mysql", dbcon())
	result := dbSum(u.TableID, db)
	db.Close()
	return c.String(http.StatusOK, result)
}

//dbSum  - Correct Data from DB
func dbSum(tableID string, db *sql.DB) string {
	stmt, _ := db.Prepare("SELECT OrData from orderRec where TableReqID = ?")
	RowLen := GetTabRecLen(db, tableID)
	data := make([]string, RowLen)
	count := 0
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("SELECT orderRec error %s\r\n", err)
		}
	}()
	rows, err := stmt.Query(tableID)
	checkErr(err)
	for rows.Next() {
		var OrData string
		err = rows.Scan(&OrData)
		data[count] = OrData
		count++
	}
	stmt.Close()
	rows.Close()
	db.Close()
	return DataPro(data)
}

//DataPro - Reconstruct the Data
func DataPro(data []string) string {
	var OrderDataSlice []OrderData
	var NewDataSlice []OrderData

	for _, e := range data {
		Getdata := []byte(e)
		json.Unmarshal(Getdata, &OrderDataSlice)
		for i := range OrderDataSlice {
			//append a new slice
			//fmt.Println("ID : " + OrderDataSlice[i].ID)
			//fmt.Println("Name : " + OrderDataSlice[i].Name)
			//fmt.Println("Org len=", len(NewDataSlice))
			if len(NewDataSlice) == 0 {
				NewDataSlice = append(NewDataSlice, OrderData{
					ID:     OrderDataSlice[i].ID,
					Name:   OrderDataSlice[i].Name,
					Amount: OrderDataSlice[i].Amount,
				})
			} else {
				indexfind := Find(NewDataSlice, OrderDataSlice[i].ID)
				//Found duplicate
				if indexfind < len(NewDataSlice) {
					org, _ := strconv.Atoi(OrderDataSlice[i].Amount)
					aft, _ := strconv.Atoi(NewDataSlice[indexfind].Amount)
					final := strconv.Itoa(org + aft)

					NewDataSlice[indexfind].Amount = final
				} else {
					NewDataSlice = append(NewDataSlice, OrderData{
						ID:     OrderDataSlice[i].ID,
						Name:   OrderDataSlice[i].Name,
						Amount: OrderDataSlice[i].Amount,
					})
				}
			}
		}
	} //end of for loop
	pushdata, _ := json.Marshal(NewDataSlice)
	result := string(pushdata)
	return result
}

//Find - for finding existing index
func Find(data []OrderData, findstr string) int {
	for i, n := range data {
		if findstr == n.ID {
			//found and return the index
			return i
		}
	}
	//didn't find and return the orignal length
	return len(data)
}
