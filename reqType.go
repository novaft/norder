package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

//reqType request by React and return the food details
func reqType(c echo.Context) error {

	ReqTypeResult := PushReqTypeResult()
	return c.String(http.StatusOK, ReqTypeResult)
}

//PushReqTypeResult DB process
func PushReqTypeResult() string {
	var result string
	db, err := sql.Open("mysql", dbcon())
	checkErr(err)
	var TDA TypeDataArray
	var pushdata []byte

	stmt, err := db.Prepare("SELECT ID,Name from FoodType")
	rows, err := stmt.Query()
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panicing %s\r\n", err)
		}
	}()
	checkErr(err)
	for rows.Next() {
		var ID string
		var Name string
		err = rows.Scan(&ID, &Name)
		TDA.TypeData = append(TDA.TypeData, TypeData{ID: ID, Name: Name})
	}
	pushdata, err = json.Marshal(TDA)
	result = string(pushdata)
	stmt.Close()
	rows.Close()

	db.Close()
	return result
}
